from datetime import datetime
import os

from flask import Blueprint, redirect, Response, request, flash, g, current_app, abort, url_for, session, render_template
from flask.ext.principal import identity_changed, Identity, AnonymousIdentity
from sqlalchemy.ext.hybrid import hybrid_property

from blog.account import frontend, backend
from blog.account.forms import LoginForm, RegisterForm, AccountForm, ProfileForm, ResetPwForm
from blog.account.models import User, Profile
from blog.extensions import db
from blog.permissions import auth, moderator, admin


### When guest comes to the blog, inquire for login or register

@frontend.route('/login', methods=['GET', 'POST'])  # request $args$ value if data is submitted as GET
def login():
    form = LoginForm(account=request.args.get('account', None),
                     next=request.args.get('next', None))  # better explicit than implicit

    if form.validate_on_submit():
        user, authenticated = User.query.authenticate(form.account.data, form.password.data)
        if user and authenticated:
            session.permanent = form.remember.data
            # represent if the user is stored/loaded from session
            identity_changed.send(current_app._get_current_object(),
                                  identity=Identity(user.id))
            flash('Welcome back, %s' % user.account, 'success')

            next_url = form.next.data

            if not next_url or next_url == request.path:
                next_url = url_for('admin_backend.index')
            return redirect(next_url)
        else:
            flash('Sorry, invalid login', 'error')
    return render_template('account/login.html', title='Sign In', form=form)


@frontend.route('/logout')
def logout():
    flash(u'You have logout', 'success')
    identity_changed.send(current_app._get_current_object(),
                          identity=AnonymousIdentity())
    next_url = request.args.get('next', '')

    if not next_url or next_url == request.path:
        next_url = url_for('account_frontend.login')
    return redirect(next_url)


@frontend.route('/register', methods=['GET', 'POST'])  # request $form$ value if data is submitted as POST
def register():
    form = RegisterForm(next=request.args.get('next', None))
    if form.validate_on_submit():  # here delete the condition request.method=='POST'
        user = User()  # form.account.data, form.email.data, form.password.data)
        form.populate_obj(user)  # populates the attributes of the passed obj with data from the form fields
        db.session.add(user)
        db.session.commit()
        flash('Thank you for registering')
        return redirect(url_for('admin_backend.index'))
    return render_template('account/register.html', form=form)


@backend.route('/<int:id>/manage', methods=['GET', 'POST'])
@auth.require(401)
def manage(id):
    user = User.query.get_or_404(id)
    profile = Profile.query.get(user.id)
#page = int(request.args.get('p',1))
#page_obj = User.query.order_by('-id').paginage(page, per_page=10)
    return render_template('account/manage.html', user=user, profile=profile)  # , page_obj=page_obj)


@backend.route('/<int:id>/manage/profile', methods=['GET', 'POST'])
@auth.require(401)
def profile(id):
    user = User.query.get_or_404(id)
    if user:
        profile = Profile(user_id=user.id)
        form = ProfileForm(first_name=profile.first_name,
                           last_name=profile.last_name,
                           addresss=profile.address,
                           city=profile.city,
                           phone=profile.phone)
    if form.validate_on_submit():
        form.populate_obj(profile)
        db.session.add(profile)
        db.session.commit()
        flash('Informations have been updated')
        return redirect(url_for('account_backend.manage', id=user.id))

    return render_template('account/profile.html', form=form)


@backend.route('/<int:id>/manage/edit', methods=['GET', 'POST'])
@auth.require(401)
def edit(id):
    user = User.query.get_or_404(id)
    form = ResetPwForm(password=request.args.get('password', None))
    if form.validate_on_submit():
        authenticated = user.check_password(form.password.data)

        if authenticated:
            user.password = form.newpassword.data
            db.session.commit()
            flash("Password is changed successfully", "success")
            next_url = url_for('account_backend.manage', id=user.id)
            return redirect(next_url)
        else:
            flash('Incorrect old password')
    return render_template('account/edit.html', form=form, user=user)
