# This is README

### 4/16 Try to finish the tag part, there are still some problems in reading the data from PostForm.
### 4/17 We can set tags for blog submission, however the unknown tag can't be updated.
### 4/18 We can show and edit tags from post view
### 4/21 All the backend function are completed. The frontend view has not been considered yet. Two functions are still needed: add image & add comment. css is not used.
### 4/24 Add comment to the blog.
### 4/25 Show the comment count on homepage, add the simplest css for the blog.
### 4/28 Change the comment count from for-loop to directly call from models. Add pagination and css
### 4/30 Upload images
