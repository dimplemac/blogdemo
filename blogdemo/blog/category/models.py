#!/flask/bin/python
#coding=utf-8

from datetime import datetime
import os
import hashlib
import json

from werkzeug import cached_property

from flask import abort, current_app, g, url_for
from flask.ext.sqlalchemy import SQLAlchemy, BaseQuery
from flask.ext.principal import RoleNeed, UserNeed, Permission

from blog.extensions import db
from blog.permissions import moderator, admin


class CatQuery(BaseQuery):
    def jsonify(self):
        for category in self.all():
            yield category.json

    def get_or_404(self, *args, **kwargs):
        try:
            return self.get(*args, **kwargs)
        except:
            abort(404)


class Category(db.Model):
    query_class = CatQuery

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    description = db.Column(db.Text)

    def __unicode__(self):
        return self.name
