import os

DEBUG = True


#SERVER_NAME = 'mytodev.cc'
SECRET_KEY = '!@#$%^&*()'
CSRF_ENABLED = True  # valid for higher security

### site info

BLOG_TITLE = "Preposterous Universe"
BLOG_URL = "http://www.myto.cc"
BLOG_NAME = "You are the chosen one"
BLOG_PERPAGE = 8
BLOG_PERARCHIVE = 20

### admin info

ADMIN_INFO = u""
ADMIN_EMAIL = "gluon.photon@gmail.com"
ADMIN_USERNAME = "GwaVe"

### login in user and password

ADMIN_USER = "dimplemac"
ADMIN_PASSWORD = "11111"
DEFAULT_TIMEZONE = "Asia/Shanghai"

### database

SQLALCHEMY_DATABASE_URI = 'mysql://root:192168@localhost/blog'
SQLALCHEMY_ECHO = False
SQLALCHEMY_POOL_RECYCLE = 7200

### upload handler

UPLOADED_PHOTOS_DEST = '/Users/GwaVe/myto/testvirtual/blogdemo/blog/static/images/'
UPLOADED_PHOTOS_URL = 'http://localhost:5002/static/images/'
UPLOADED_PHOTOS_ALLOW = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
