#!/flask/bin/python
#coding=utf-8

from datetime import datetime
import os
import json

from flask_wtf import Form
from wtforms import TextField, BooleanField, HiddenField, PasswordField, \
    SubmitField, TextAreaField, validators
from wtforms.validators import required, EqualTo, email, ValidationError

from flask import Flask, Blueprint, redirect, Response, request, flash, g, \
    current_app, abort, url_for, session, render_template, jsonify
# from flask.ext.principal import identity_changed, Identity, AnonymousIdentity

from blog.extensions import db
from blog.permissions import auth, moderator, admin

from blog.account.models import User
from blog.post.models import Post

from blog.comment.forms import CommentForm
from blog.comment.models import Comment
from blog.comment import frontend, backend


@frontend.route('/post/<int:post_id>/comment/new', methods=['GET', 'POST'])
def new(post_id):
    form = CommentForm(author=g.user)
    if form.validate_on_submit():
        comment = Comment(post_id=post_id)
        form.populate_obj(comment)

        if g.user:
            comment.author = g.user

        db.session.add(comment)
        db.session.commit()
        flash('Submit success', 'success')
        return redirect(url_for('home_frontend.view', id=comment.post_id))

    return render_template('comment/new.html', form=form)


@frontend.route('/post/comment/<int:id>/edit', methods=['GET', 'POST'])
def edit(id):
    comment = Comment.query.get_or_404(id)
    form = CommentForm(request.form, comment)
    if form.validate_on_submit():
        form.populate_obj(comment)

        if g.user:
            comment.author = g.user

        db.session.add(comment)
        db.session.commit()
        flash('Submit success', 'success')
        return redirect(url_for('home_frontend.view', id=comment.post_id))

    return render_template('comment/edit.html', form=form)


@frontend.route("/post/comment/<int:id>/delete", methods=['GET', 'POST'])
def delete(id):
    comment = Comment.query.get_or_404(id)
    postid = comment.post_id
    db.session.delete(comment)
    db.session.commit()

    return redirect(url_for('home_frontend.view', id=postid))
