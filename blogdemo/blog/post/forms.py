#!/flask/bin/python
#coding=utf-8

from flask_wtf import Form
from wtforms import TextField, BooleanField, HiddenField, PasswordField, \
    SubmitField, TextAreaField, SelectField, validators
from wtforms.validators import Required, EqualTo, email, ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from blog.extensions import db

from blog.account.models import User
from blog.post.models import Post
from blog.category.models import Category


def category_choice():
    return Category.query.all()


class PostForm(Form):
    title = TextField(u'Title', [validators.required()])
    thumb = HiddenField(u'Thumb')
    category = QuerySelectField(u'Category', query_factory=category_choice)
    tags = TextField(u'Tag')
    content = TextAreaField(u'Content')
    submit = SubmitField(u'Submit')

"""
    def validate_slug(self, field):
        if len(field.data) > 50:
            raise ValidationError("Slug must be less than 50 characters")
        slug = slugify(field.data) if field.data else slugify(self.title.data)[:50]
        post = Post.query.filter_by(slug=slug)
        if self.post:
            post = post.filter(db.not_(Post.id==self.post.id))
        if post.count():
            error("This slug is taken") if field.data else "slug is required"
            raise ValidationError, error
"""
