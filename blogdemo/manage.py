#!flask/bin/python
from flask import Flask, current_app
from flask.ext.script import Manager, Server, Shell, Command

from blog import create_app
from blog.extensions import db
from blog.account.models import User

manager = Manager(create_app('config.py'))

manager.add_command("runserver", Server('127.0.0.1',port=5002))

def _make_context():
	return dict(db=db)
manager.add_command("shell", Shell(make_context=_make_context))

@manager.command
def createall():
	"creates database tables"
	db.create_all()

if __name__ == "__main__":
	manager.run()
