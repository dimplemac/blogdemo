#!/flask/bin/python
#coding=utf-8

from flask_wtf import Form
from wtforms import TextField, BooleanField, HiddenField, PasswordField, \
    SubmitField, TextAreaField, SelectField, validators
from wtforms.validators import Required, EqualTo, email, ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from blog.extensions import db

from blog.account.models import User
from blog.post.models import Post
from blog.tag.models import Tag


class TagForm(Form):
    name = TextField('TagName', [validators.required(), validators.length(min=1, max=60)])
    next = HiddenField()
    