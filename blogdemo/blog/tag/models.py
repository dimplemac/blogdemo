#!/flask/bin/python
#coding=utf-8

from datetime import datetime
import os
import hashlib
import json

from werkzeug import cached_property

from flask import abort, current_app, g, url_for
from flask.ext.sqlalchemy import SQLAlchemy, BaseQuery
from flask.ext.principal import RoleNeed, UserNeed, Permission

from blog.extensions import db
from blog.permissions import moderator, admin


class TagQuery(BaseQuery):
    def jsonify(self):
        for tag in self.all():
            yield tag.json

    def get_or_404(self, *args, **kwargs):
        try:
            return self.get(*args, **kwargs)
        except:
            abort(404)


class Tag(db.Model):
    query_class = TagQuery

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)

    def __unicode__(self):
        return self.name


# class TagPost(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     postid = db.Column(db.Integer, db.ForeignKey('post.id', ondelete='CASCADE'), nullable=False)
#     tagid = db.Column(db.Integer, db.ForeignKey('tag.id'), nullable=False)

#     post = db.relationship('Post', backref='TagQuery', lazy='joined')
#     tag = db.relationship('Tag', backref='TagQuery', lazy='joined')
