#!/flask/bin/python
#coding=utf-8

from datetime import datetime
import os
import hashlib
import json

from werkzeug import cached_property

from flask import abort, current_app, g, url_for
from flask.ext.sqlalchemy import SQLAlchemy, BaseQuery
from flask.ext.principal import RoleNeed, UserNeed, Permission

from blog.extensions import db
from blog.permissions import moderator, admin

from blog.account.models import User
from blog.post.models import Post


class CommentQuery(BaseQuery):
    def jsonify(self):
        for comment in self.all():
            yield comment.json

    def get_or_404(self, *args, **kwargs):
        try:
            return self.get(*args, **kwargs)
        except:
            abort(404)


class Comment(db.Model):
    query_class = CommentQuery

    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer,
                        db.ForeignKey(Post.id, ondelete='CASCADE'),
                        nullable=False)
    user_id = db.Column(db.Integer,
                        db.ForeignKey(User.id, ondelete='CASCADE'))

    content = db.Column(db.UnicodeText)
    created_date = db.Column(db.DateTime, default=datetime.utcnow)

    author = db.relationship('User', innerjoin=True, backref='User', lazy='joined')
    post = db.relationship('Post', innerjoin=True, backref='Post', lazy="joined")

    # __mapper_args__ = {'order_by' : id.asc()}
    class Permissions(object):

        def __init__(self, obj):
            self.obj = obj

        @cached_property
        def reply(self):
            return Permission(UserNeed(self.obj.post.user_id))

        @cached_property
        def delete(self):
            return Permission(UserNeed(self.obj.user_id),
                              UserNeed(self.obj.post.user_id)) & moderator

    def __init__(self, *args, **kwargs):
        super(Comment, self).__init__(*args, **kwargs)

    @cached_property
    def permissions(self):
        return self.Permissions(self)
