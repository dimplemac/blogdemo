#!/flask/bin/python
#coding=utf-8

from flask_wtf import Form
from wtforms import TextField, BooleanField, HiddenField, PasswordField, SubmitField, TextAreaField, validators
from wtforms.validators import required, EqualTo, email, ValidationError

from flask import Flask, Blueprint, redirect, Response, request, flash, g, current_app, abort, url_for, session, render_template

from blog.extensions import db, photos
from blog.permissions import auth, moderator, admin

from blog.admin import backend

from blog.account.models import User
from blog.post.models import Post


@backend.route('/', methods=['GET', 'POST'])
# @auth.required(401)
def index():
    return render_template('admin/index.html')
