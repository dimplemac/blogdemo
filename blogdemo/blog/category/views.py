#!/flask/bin/python
#coding=utf-8

from datetime import datetime
import os
import json

from flask_wtf import Form
from wtforms import TextField, BooleanField, HiddenField, PasswordField, SubmitField, TextAreaField, validators
from wtforms.validators import required, EqualTo, email, ValidationError

from flask import Flask, Blueprint, redirect, Response, request, flash, g, current_app, abort, url_for, session, render_template
# from flask.ext.principal import identity_changed, Identity, AnonymousIdentity

from blog.extensions import db, photos
from blog.permissions import auth, moderator, admin

from blog.account.models import User
from blog.post.models import Post

from blog.category import backend, frontend
from blog.category.models import Category
from blog.category.forms import CategoryForm


@backend.route('/category', methods=['GET', 'POST'])
@auth.require(401)
def index():
    category = Category.query.all()
    return render_template('category/index.html', category=category)


@backend.route('/category/new', methods=['GET', 'POST'])
@auth.require(401)
def new():
    catlist = Category.query.all()
    form = CategoryForm()
    category = Category()
    if form.validate_on_submit():
        form.populate_obj(category)
        db.session.add(category)
        db.session.commit()
        return redirect(url_for('category_backend.index'))
    return render_template('category/new.html', form=form, catlist=catlist)


@backend.route('/category/<int:id>/edit', methods=['GET', 'POST'])
@auth.require(401)
def edit(id):
    category = Category.query.get_or_404(id)
    if not category:
        return 404
    form = CategoryForm(request.form, category)

    if form.validate_on_submit():
        form.populate_obj(category)
        db.session.add(category)
        db.session.commit()
        return redirect(url_for('category_backend.index'))
    return render_template('category/edit.html', form=form)


@backend.route('category/<int:id>/delete', methods=['GET', 'POST'])
@auth.require(401)
def delete(id):
    category = Category.query.get_or_404(id)
    db.session.delete(category)
    db.session.commit()
    return redirect(url_for('category_backend.index'))


@backend.route('/category/<name>', methods=['GET', 'POST'])
def view(name):
    post = Post.query.filter(Post.category_name == name).filter(Post.user_id == g.user.id).all()
    return render_template('category/view.html', post=post)
