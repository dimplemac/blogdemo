#!flask/bin/python
#coding:utf-8

import os
import json
import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
import datetime

from werkzeug import import_string
from flask import Flask, g, session, request, flash, redirect, jsonify, url_for, render_template
from flask.ext.principal import Principal, RoleNeed, UserNeed, identity_loaded
from flaskext.uploads import configure_uploads

from blog.extensions import db, mail, photos
from blog.account.models import User
#from blog.post.models import Post

DEFAULT_APP_NAME = 'blog'

### register the imported modules here, using strings, the modules import are shown later
DEFAULT_BLUEPRINTS = ['blog.account:frontend',
                      'blog.account:backend',
                      'blog.post:frontend',
                      'blog.post:backend',
                      'blog.category:frontend',
                      'blog.category:backend',
                      'blog.tag:frontend',
                      'blog.tag:backend',
                      'blog.comment:frontend',
                      'blog.comment:backend',
                      'blog.home:frontend',
                      'blog.admin:backend'
                      ]


def create_app(config=None, blueprints=None):

    if blueprints is None:
        blueprints = DEFAULT_BLUEPRINTS
    app = Flask(DEFAULT_APP_NAME)
    app.config.from_pyfile(config)

    # extension for db creation
    configure_extensions(app)
    configure_identity(app)
    # configure_logging(app)
    configure_before_handlers(app)

    # upload & set the max size
    configure_uploads(app, (photos,))
    # patch_request_class(app, size=16777216)

    # register the module to app, the last one of all the configure
    configure_blueprints(app, blueprints)

    return app


### register modules with app, without url_prefix for now; and import modules into the package string

def configure_blueprints(app, blueprints):
    blueprints_list = []
    packages_list = []

    for name in blueprints:
        # put module into blueprint
        blueprint = import_string(name)
        blueprints_list.append(blueprint)
        # print blueprint.import_name
        package = import_string(blueprint.import_name)
        packages_list.append(package)

#   if isinstance(packages_list,(str,list)):
#       return packages_list
#   else: packages_list=packages_list.split()

    for package in list(set(packages_list)):
        for member_name in getattr(package, '__all__', []):  # get obj.name from __all__ py files
            __import__('%s.%s' % (package.__name__, member_name))

    for blueprint in list(set(blueprints_list)):
        app.register_blueprint(blueprint)


def configure_extensions(app):
    db.init_app(app)
    mail.init_app(app)


### define g.user

def configure_identity(app):

    principal = Principal(app)

    @identity_loaded.connect_via(app)
    def on_identity_loaded(sender, identity):
        g.user = User.query.from_identity(identity)


### call g thread local from authenticated user

def configure_before_handlers(app):
    @app.before_request
    def authenticate():
        g.user = getattr(g.identity, 'user', None)
