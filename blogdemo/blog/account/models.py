#!/flask/bin/python
#coding=utf-8

from random import choice
from datetime import datetime
import os
import hashlib

from werkzeug import cached_property
from flask import abort, current_app, g

from flask.ext.sqlalchemy import SQLAlchemy, BaseQuery
from flask.ext.principal import RoleNeed, UserNeed, Permission
from sqlalchemy.ext.hybrid import hybrid_property

from blog.extensions import db
from blog.permissions import admin

password_secret = 'lawliet'


def create_token(length=16):
    chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    salt = ''.join([choice(chars) for i in range(length)])
    return salt


class UserQuery(BaseQuery):
    def authenticate(self, account, password):
        user = self.filter(db.or_(User.account == account)).first()
        if user:
            authenticated = user.check_password(password)
        else:
            authenticated = False
        return user, authenticated

    def search(self, key):
        query = self.filter(db.or_(User.email == key,
                            User.account.ilike('%' + key + '%')))
        return query

    def get_by_username(self, account):
        user = self.filter(User.account == account).first()
        if user is None:
            abort(404)
        return user

    def from_identity(self, identity):
        try:
            user = self.get(identity.id) if identity.id else None
        except ValueError:
            user = None

        if user:
            identity.provides.update(user.provides)
        identity.user = user
        return user


class User(db.Model):
    query_class = UserQuery

    USER = 0
    ADMIN = 1

    # if use __tablename__ = '', it is best to use the same name with class

    id = db.Column(db.Integer, primary_key=True)
    account = db.Column(db.String(20), unique=True, nullable=False)
    nickname = db.Column(db.String(20), index=True, unique=True)
    email = db.Column(db.String(100), index=True, unique=True)
    _password = db.Column('password', db.String(100), nullable=False)
    role = db.Column(db.SmallInteger, default=USER)
    active = db.Column(db.Boolean, default=False)
    created_date = db.Column(db.DateTime, default=datetime.utcnow)

    profile = db.relationship('Profile', backref='user', lazy='dynamic')
    post = db.relationship('Post', backref='user', lazy='dynamic')

    class Permissions(object):

        def __init__(self, obj):
            self.obj = obj

        @cached_property
        def edit(self):
            return Permission(UserNeed(self.obj.id)) & admin

    #super return True while (type, type2) or (type, object)
    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.account

    def __repr__(self):
        return '<%s>' % self

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        salt = create_token(8)
        hsh = hashlib.sha1(salt + password + password_secret).hexdigest()
        self._password = '%s$%s' % (salt, hsh)

    def check_password(self, password):
        if self.password is None:
            return False
        salt, hsh = self.password.split('$')
        verify = hashlib.sha1(salt + password + password_secret).hexdigest()
        return verify == hsh

    @cached_property
    def provides(self):
        needs = [RoleNeed('authenticated'), UserNeed(self.id)]
        if self.is_admin:
            needs.append(RoleNeed('admin'))
        return needs

    @property
    def is_admin(self):
        return self.role == self.ADMIN


### edit the profile of the user

class Profile(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, unique=True)
    first_name = db.Column(db.String(30))
    last_name = db.Column(db.String(30))
    address = db.Column(db.Text)
    city = db.Column(db.Text)
    phone = db.Column(db.String(30))
    updated = db.Column(db.DateTime, default=datetime.utcnow,
                        onupdate=datetime.utcnow)
