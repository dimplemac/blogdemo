#!flask/bin/python
#coding=utf-8
"""
define all the modules used in post, to make it easy to use the modules when
 register it.
"""

from flask import Blueprint

__all__ = ['views', 'models']

frontend = Blueprint('tag_frontend', __name__, template_folder='templates')
backend = Blueprint('tag_backend', __name__, url_prefix='/admin', template_folder='templates')
