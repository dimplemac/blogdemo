#!/flask/bin/python
#coding=utf-8

from datetime import datetime
import os
import json

from flask_wtf import Form
from wtforms import TextField, BooleanField, HiddenField, PasswordField, SubmitField, TextAreaField, validators
from wtforms.validators import required, EqualTo, email, ValidationError

from flask import Flask, Blueprint, redirect, Response, request, flash, g, current_app, abort, url_for, session, render_template
# from flask.ext.principal import identity_changed, Identity, AnonymousIdentity

from blog.extensions import db, photos
from blog.permissions import auth, moderator, admin

from blog.account.models import User
from blog.post.models import Post

from blog.tag import backend, frontend
from blog.tag.models import Tag
from blog.tag.forms import TagForm


@backend.route('/tag', methods=['GET', 'POST'])
@auth.require(401)
def index():
    # post = Post.query.filter(Post.user_id == g.user.id).all()

    tag = Tag.query.all()
    return render_template('tag/index.html', tag=tag)


@backend.route('/tag/new', methods=['GET', 'POST'])
@auth.require(401)
def new():
    taglist = Tag.query.all()
    form = TagForm()
    tag = Tag()
    if form.validate_on_submit():
        form.populate_obj(tag)
        db.session.add(tag)
        db.session.commit()
        return redirect(url_for('tag_backend.index'))
    return render_template('tag/new.html', form=form, taglist=taglist)


@backend.route('/tag/<name>', methods=['GET', 'POST'])
def view(name):
    post = Post.query.filter(Post.tag.any(Tag.name == name)).filter(Post.user_id == g.user.id).all()
    # http://stackoverflow.com/questions/21840012/sqlalchemy-select-all-posts-that-has-tags-in-many-to-many
    return render_template('tag/view.html', post=post)
