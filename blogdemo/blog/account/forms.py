#!/flask/bin/python
#coding=utf-8

from flask_wtf import Form
from wtforms import TextField, BooleanField, HiddenField, PasswordField, SubmitField, TextAreaField, validators
from wtforms.validators import required, EqualTo, email, ValidationError
from flask import Flask
from flask.ext.babel import gettext

from blog.account.models import User
from blog.extensions import db


class LoginForm(Form):
    account = TextField(u'Username', [validators.required()])
    password = PasswordField(u'Password')
    remember = BooleanField(u'remember_me', default=False)
    next = HiddenField()
    submit = SubmitField('Login')


class RegisterForm(Form):
    account = TextField(u'Username', [validators.required(),
                                      validators.length(min=4, max=25)])
    nickname = TextField(u'Nickname', [validators.required()])
    email = TextField(u'Email Address', [validators.required(),
                                         validators.length(min=6, max=35)])
    password = PasswordField(u'Password', [
        validators.required(),
        validators.EqualTo(u'confirm', message='Passwords must match')
    ])
    confirm = PasswordField(u'Repeat Password')
    accept_tos = BooleanField(u'I accept the TOS', [validators.required()])
    next = HiddenField()
    submit = SubmitField('Create account')

    def validate_username(self, field):
        user = User.query.filter(User.account.like(field.data)).first()
        if user:
            raise ValidationError("The username is existing")

    def validate_email(self, field):
        user = User.query.filter(User.email.like(field.data)).first()
        if user:
            raise ValidationError("The email is existing")


class AccountForm(Form):
    account = TextField(u'Username', [validators.required()])
    email = TextField(u'Email Address', [validators.required()])
    submit = SubmitField('Save')


class ProfileForm(Form):
    first_name = TextField(u'First Name', [validators.required()])
    last_name = TextField(u'Last Name', [validators.required()])
    address = TextField(u'Address', [validators.required()])
    city = TextField(u'city', [validators.required()])
    phone = TextField(u'Phone', [validators.required()])
    submit = SubmitField('Save')


class ResetPwForm(Form):
    password = PasswordField(u'Old Password', [validators.required()])
    newpassword = PasswordField(u'New Password', [validators.required()])
    confirm = PasswordField(u'Repeat New Password', [
        validators.required(),
        validators.EqualTo('newpassword', message='Passwords must match')])
    submit = SubmitField('Submit')
