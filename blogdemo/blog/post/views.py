#!/flask/bin/python
#coding=utf-8

from datetime import datetime
import os
import json

from flask_wtf import Form
from wtforms import TextField, BooleanField, HiddenField, PasswordField, SubmitField, TextAreaField, validators
from wtforms.validators import required, EqualTo, email, ValidationError

from flask import Flask, Blueprint, redirect, Response, request, flash, g, current_app, abort, url_for, session, render_template
# from flask.ext.principal import identity_changed, Identity, AnonymousIdentity

from blog.extensions import db, photos
from blog.permissions import auth, moderator, admin

from blog.post import backend, frontend
from blog.post.forms import PostForm
from blog.post.models import Post

from blog.account.models import User
from blog.tag.models import Tag
from blog.comment.models import Comment


@backend.route('/post', methods=['GET', 'POST'])
@auth.require(401)
def index():
    page = int(request.args.get('p', 1))
    post = Post.query.filter(Post.user_id == g.user.id).paginate(page, per_page=10)
    return render_template('post/index.html', page_obj=post)


@backend.route('/post/<int:id>', methods=['GET', 'POST'])
@auth.require(401)
def view(id):
    post = Post.query.get_or_404(id)
    comment = Comment.query.filter(Comment.post_id == post.id)
    return render_template('post/view.html', post=post, comment=comment)


@backend.route('/post/new', methods=['GET', 'POST'])
@auth.require(401)
def new():
    form = PostForm()

    if request.method == "POST" and form.validate_on_submit():
        # Post.create(**form.data)
        post = Post(author=g.user)
        tags = form.tags.data.split(',')
        for t in tags:
            tag = Tag.query.filter_by(name=t.strip()).first()
            if not tag:
                tag = Tag(name=t.strip())
            post.tag.append(tag)

        form.populate_obj(post)
        db.session.add(post)
        db.session.commit()
        flash('Submit success', 'success')
        return redirect(url_for('post_backend.index'))
    return render_template('/post/new.html', form=form)


@backend.route('/post/<int:id>/edit', methods=['GET', 'POST'])
@auth.require(401)
def edit(id):
    post = Post.query.get_or_404(id)

    newtag = []
    if not post:
        return 404
    form = PostForm(request.form, post)

    if form.validate_on_submit():
        if form.tags.data:
            tags = form.tags.data.split(',')

            for t in tags:
                tag = Tag.query.filter_by(name=t.strip()).first()
                if not tag:
                    tag = Tag(name=t.strip())
                newtag.append(tag)

            for p in post.tag:
                if p not in newtag:
                    post.tag.remove(p)

            post.tag.extend(newtag)

        form.populate_obj(post)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('post_backend.view', id=post.id))
    return render_template('post/edit.html', form=form, post=post)


@backend.route('/post/<int:id>/delete', methods=['GET', 'POST'])
@auth.require(401)
def delete(id):
    post = Post.query.get_or_404(id)
    db.session.delete(post)
    db.session.commit()
    return redirect(url_for('post_backend.index'))


@backend.route('/upload', methods=['POST'])
def upload():
    if request.method == 'POST' and 'imgFile' in request.files:
        filename = photos.save(request.files['imgFile'])
        flash("Photo saved.")
        return json.dumps({"error": 0, "url": photos.url(filename)})
    return json.dumps({"error": 1, "message": "Wrong Message"})


## save uploaded files to url


# @backend.route('/photo/<id>')
# def show(id):
#     photo = Photo.load(id)
#     if photo is None:
#         abort(404)
#     url = photos.url(photo.filename)
#     return render_template('post/show.html', url=url, photo=photo)
