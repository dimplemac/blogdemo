#!flask/bin/python
# coding=utf-8

import os
import re
import hashlib
import uuid

from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.mail import Mail
from flaskext.uploads import UploadSet, IMAGES

__all__ = ['db', 'mail', 'photos']

db = SQLAlchemy()
mail = Mail()

photos = UploadSet('photos', IMAGES)


# def get_photo_path_name():
#     random_str = hashlib.md5(str(uuid.uuid1())).hexdigest()
#     return ('%s/%s' % (random_str[0:2], random_str[2:4]), random_str[4:])


# def save_image(data, kind=None, name=None):
#     photo_path, photo_name = get_photo_path_name()
#     name = name if name else photo_name
#     path = '%s/%s' % (kind, photo_path) if kind else photo_path
#     photoname = photos.save(data, folder=path, name='%s.' % name)
#     # extension_name = photoname.split('.')[1]

#     return photoname.split(kind + '/')[1] if kind else photoname


# def _handleUpload(files):
#     if not files:
#         return None

#     filenames = []
#     saved_files_urls = []

#     for key, file in files.iteritems():
#         if file and allowed_file(file.filename):
#             filename = secure_filename(file.filename)
#             print os.path.join(UPLOAD_FOLDER, filename)
#             file.save(os.path.join(UPLOAD_FOLDER, filename))
#             saved_files_urls.append(url_for('uploaded_file', filename=filename))

#             filenames.append("%s" % (file.filename))
#             print saved_files_urls[0]

#     return filenames


# class PostMixin(object):
#     id = db.Column(db.Integer, primary_key=True)

#     @classmethod
#     def create(cls, commit=True, **kwargs):
#         instance = cls(**kwargs)
#         return instance.save(commit=commit)

#     @classmethod
#     def get(cls, id):
#         return cls.query.get(id)

#     @classmethod
#     def get_or_404(cls, id):
#         return cls.query.get_or_404(id)

#     def update(self, commit=True, **kwargs):
#         for attr, value in kwargs.iteritems():
#             setattr(self, attr, value)
#         return commit and self.save() or self

#     def save(self, commit=True):
#         db.session.add(self)
#         if commit:
#             db.session.commit()
#         return self

#     def delete(self, commit=True):
#         db.session.delete(self)
#         return commit and db.session.commit()
