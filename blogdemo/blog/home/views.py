#!/flask/bin/python
#coding=utf-8

from datetime import datetime
import os
import json

from flask_wtf import Form
from wtforms import TextField, BooleanField, HiddenField, PasswordField, SubmitField, TextAreaField, validators
from wtforms.validators import required, EqualTo, email, ValidationError

from flask import Flask, Blueprint, redirect, Response, request, flash, g, \
    current_app, abort, url_for, session, render_template
from sqlalchemy import desc
# from flask.ext.principal import identity_changed, Identity, AnonymousIdentity

from blog.extensions import db, photos
from blog.permissions import auth, moderator, admin

from blog.account.models import User
from blog.post.models import Post
from blog.category.models import Category
from blog.tag.models import Tag
from blog.comment.models import Comment  # , comment_count

from blog.home import frontend


@frontend.route('/', methods=['GET', 'POST'])
@frontend.route('/index', methods=['GET', 'POST'])
def index():
    page = int(request.args.get('p', 1))
    post = Post.query.order_by(desc(Post.created_date)).paginate(page, per_page=5)
    # post = Post.query.order_by(desc(Post.created_date)).all()

    return render_template('home/index.html', page_obj=post)


@frontend.route('/post/<int:id>', methods=['GET', 'POST'])
def view(id):
    post = Post.query.get_or_404(id)
    comment = Comment.query.filter(Comment.post_id == post.id)
    return render_template('home/view.html', post=post, comment=comment)


@frontend.route('/error', methods=['GET', 'POST'])
def error():
    return render_template('home/error.html')

# @frontend.route('/post/<category>', methods=['GET', 'POST'])
# def list_by_cat(category):
#     post = Post.query.filter(Post.category_name == category).all()
#     return render_template('list_by_cat.html', post=post)
