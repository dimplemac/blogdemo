#!/flask/bin/python
#coding=utf-8

from flask_wtf import Form
from wtforms import TextField, BooleanField, HiddenField, PasswordField, \
    SubmitField, TextAreaField, SelectField, validators
from wtforms.validators import Required, EqualTo, email, ValidationError

from blog.extensions import db

from blog.account.models import User
from blog.post.models import Post
from blog.comment.models import Comment


class CommentForm(Form):
    nickname = TextField(u'Nickname')
    email = TextField(u'Email')
    content = TextAreaField(u'Content', [validators.required()])
    submit = SubmitField(u'Add comment')
    cancel = SubmitField(u'Cancel')
    next = HiddenField()
