#!flask/bin/python
#coding=utf-8
"""
define all the modules used in post, to make it easy to use the modules when
 register it.
"""

from flask import Blueprint

__all__ = ['views', 'models']

frontend = Blueprint('home_frontend', __name__, template_folder='templates')
