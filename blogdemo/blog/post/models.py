#!/flask/bin/python
#coding=utf-8

from datetime import datetime
import os
import hashlib
import json

from werkzeug import cached_property

from flask import abort, current_app, g, url_for
from flask.ext.sqlalchemy import SQLAlchemy, BaseQuery
from flask.ext.principal import RoleNeed, UserNeed, Permission

from blog.extensions import db, photos
from blog.permissions import moderator, admin

from blog.account.models import User
from blog.category.models import Category
from blog.tag.models import Tag


### help table for many to many relationship

assoc_table = db.Table(
    'association',
    db.Column('postid', db.Integer, db.ForeignKey('post.id')),
    db.Column('tagid', db.Integer, db.ForeignKey('tag.id'))
)


class PostQuery(BaseQuery):
    def jsonify(self):
        for post in self.all():
            yield post.json

    def get_or_404(self, *args, **kwargs):
        try:
            return self.get(*args, **kwargs)
        except:
            abort(404)

#     def archive(self, year, month, day):
#         if not year:
#             return self

#         criteria = []
#         criteria.append(db.extract('year', Post.created_date) == year)
#         if month:
#             criteria.append(db.extract('month', Post.created_date) == month)
#         if day:
#             criteria.append(db.extract('day', Post.created_date) == day)

#         q = reduce(db.and_, criteria)
#         return self.filter(q)


class Post(db.Model):  # (PostMixin, db.Model):
    query_class = PostQuery
    #PER_PAGE = 40

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'), nullable=False)

    author = db.relationship('User', innerjoin=True, backref='Post', lazy='joined')

    title = db.Column(db.String(140), index=True)
    # _thumb = db.Column('thumb', db.String(40))
    content = db.Column(db.UnicodeText)
    created_date = db.Column(db.DateTime, default=datetime.utcnow)
    update_time = db.Column(db.DateTime, default=datetime.utcnow)

    category_name = db.Column(db.String(10), db.ForeignKey('category.name', ondelete='CASCADE'))
    category = db.relationship('Category', backref='Post', lazy='joined')

    tag = db.relationship('Tag', secondary=assoc_table, backref='Post')
    comment = db.relationship('Comment', backref='Post', lazy="joined")

    class Permissions(object):

        def __init__(self, obj):
            self.obj = obj

        @cached_property
        def edit(self):
            return Permission(UserNeed(self.obj.post_id))

        @cached_property
        def delete(self):
            return Permission(UserNeed(self.obj.post_id)) & moderator

    def __init__(self, *args, **kwargs):  # super return True while (type, type2) or (type, object)
        super(Post, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.title

    def __repr__(self):
        return '<%s>' % self

    @cached_property
    def permissions(self):
        return self.Permissions(self)

    # def allowed_file(filename):
    #     return '.' in filename and \
    #        filename.rsplit('.', 1)[1] in UPLOADED_FILES_ALLOW
